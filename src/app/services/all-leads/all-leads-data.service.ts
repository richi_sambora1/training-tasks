import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { allLeadsData } from 'src/app/shared/models/allLeadsData';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})


export class AllLeadsDataService {

  constructor(
    private http: HttpClient
  ) { }

  public postLoginResponse$(loginUrl: string, loginData: object) {
    return this.http
      .post(loginUrl, loginData)
      .pipe(
        map(data => { return data }))
  }

  public getAllLeadsData$(allLeadsUrl: string, accessToken: string): Observable<allLeadsData[]> {
    return this.http
      .get<allLeadsData[]>(allLeadsUrl, {
        headers: {
          Authorization: 'Bearer ' + accessToken
        }
      })
      .pipe(
        map(data => {
          return data;
        })
      )
  }
}

