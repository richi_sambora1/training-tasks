import { SECTION } from './sections';
import { TableComponent } from './view/v-table.component';

export const MAINWINDOWCOMPONENTS = [
    SECTION,
    TableComponent,
];