import { Component, OnInit } from '@angular/core';

import groupsData from 'src/assets/workspaces_analyzed.json';
import { AllLeadsDataService } from 'src/app/services/all-leads/all-leads-data.service';

@Component({
  selector: 'table-component',
  templateUrl: './v-table.component.html'
})

export class TableComponent implements OnInit {
  public title: string = "Slack community groups";
  public Groups: any = groupsData;
  public headers = ["name", "members", "channels", "bots", "link", "in_excel"];
  public headers_views = ["Name", "Members", "Channels", "Bots", "Link", "Excel"];

  public accessToken: string;
  public dataSet: string;
  public dataSet1: string;
  public allLeadsData: object;
  public loginUrl: string = 'https://lgt-billing.disposed.xyz:/api/login';
  public registerUrl: string = 'https://lgt-billing.disposed.xyz:/api/register';
  public allLeadsUrl: string = 'https://lgt-billing.disposed.xyz:443/api/lead_list?skip=0&limit=25';

  public registerData = {
    email: "nikita.sufranovich313@gmail.com",
    password: "detroit3131",
    user_name: "nekit228",
    company: "KBP",
    position: "headmaster"
  }

  public loginData = {
    email: "nikita.sufranovich313@gmail.com",
    password: "detroit3131"
  }

  private delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  constructor(
    public allLeadsService: AllLeadsDataService
  ) { }

  async ngOnInit() {
    this.allLeadsService.postLoginResponse$(this.loginUrl, this.loginData)
      .subscribe((data) => {
        this.dataSet = JSON.stringify(data);
        this.dataSet1 = JSON.parse(this.dataSet);
        this.accessToken = this.dataSet1["access_token"];
      });
      
    await this.delay(5000);
    this.allLeadsService.getAllLeadsData$(this.allLeadsUrl, this.accessToken)
      .subscribe((data) => {
        this.allLeadsData = data;
      });
    await this.delay(5000);
    console.log(this.allLeadsData);
  }
}
