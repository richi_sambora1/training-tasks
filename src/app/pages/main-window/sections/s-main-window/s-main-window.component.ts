import { Component, Input } from '@angular/core';

@Component({
  selector: 'main-window',
  templateUrl: './s-main-window.component.html',
  styleUrls: ['./s-main-window.component.scss']
})
export class MainWindowComponent {
  @Input() public title: string;
  @Input() public Groups: any;
  @Input() public headers: [string];
  @Input() public headers_views: [string];
  @Input() public response: string;

  public page: Number = 1;
  public countOfRecords: Number = 10;
}
