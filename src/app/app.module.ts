import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { NgxPaginationModule } from 'ngx-pagination';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainWindowComponent } from './pages/main-window/sections/s-main-window/s-main-window.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { MAINWINDOWCOMPONENTS } from './pages/main-window';
import { TableComponent } from './pages/main-window/view/v-table.component';
import { AllLeadsDataService } from './services/all-leads/all-leads-data.service';

@NgModule({
  declarations: [
    AppComponent,
    MainWindowComponent,
    MAINWINDOWCOMPONENTS,
    HeaderComponent,
    TableComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxPaginationModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [AllLeadsDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
