export interface allLeadsData {
    id: string;
    text: string;
    notes: string;
    status: string;
    url: string;
}